FROM openjdk:8

WORKDIR /work/sytac/

VOLUME /work/sytac/output/

COPY sytac-twitter-stream/target/sytac-twitter-stream.fat.jar .

ENTRYPOINT java -jar sytac-twitter-stream.fat.jar --consumer-key=$CONSUMER_KEY \
--consumer-secret=$CONSUMER_SECRET --token=$TOKEN --token-secret=$TOKEN_SECRET \
--keywords=$KEYWORDS --timeout=$TIMEOUT --max=$MAX_TWITS
