package victor.sytac.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import victor.sytac.json.input.Twit;
import victor.sytac.json.input.TwitterUser;
import victor.sytac.json.output.OutputUserTwits;

class TwitOutputServiceTest {

  private TwitOutputService service;

  @BeforeEach
  void before() {
    service = new TwitOutputService();
  }

  @Test
  void convertUserTwitMapToOutputFormat_null_emptySet() {
    Set<OutputUserTwits> result = service.convertUserTwitMapToOutputFormat(null);

    assertTrue(result.isEmpty());
  }

  @Test
  void convertUserTwitMapToOutputFormat_someTwits_success() {
    Map<TwitterUser, List<Twit>> userTwitMap = new HashMap<>();

    TwitterUser user1 =
        new TwitterUser(
            "id_01", new Date(System.currentTimeMillis() - 60_000), "user 01", "user01");
    TwitterUser user2 = new TwitterUser("id_02", new Date(), "user 02", "user02");

    userTwitMap
        .computeIfAbsent(user1, twitterUser -> new LinkedList<>())
        .add(new Twit("id_01", "msg01", new Date(System.currentTimeMillis() - 30_000), user1));
    userTwitMap
        .computeIfAbsent(user1, twitterUser -> new LinkedList<>())
        .add(new Twit("id_02", "msg02", new Date(System.currentTimeMillis() - 15_000), user1));
    userTwitMap
        .computeIfAbsent(user2, twitterUser -> new LinkedList<>())
        .add(new Twit("id_03", "msg03", new Date(), user2));

    Set<OutputUserTwits> result = service.convertUserTwitMapToOutputFormat(userTwitMap);
    Iterator<OutputUserTwits> iterator = result.iterator();

    assertEquals(2, result.size());
    assertEquals(2, iterator.next().getTwits().size());
    assertEquals(1, iterator.next().getTwits().size());
  }
}
