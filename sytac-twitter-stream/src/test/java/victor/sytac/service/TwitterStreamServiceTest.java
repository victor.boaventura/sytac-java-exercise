package victor.sytac.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verifyZeroInteractions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.httpclient.BasicClient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import victor.sytac.config.TwitterClientConfig;
import victor.sytac.exception.TwitterStreamingException;
import victor.sytac.json.input.Twit;
import victor.sytac.json.input.TwitterUser;

@ExtendWith(MockitoExtension.class)
class TwitterStreamServiceTest {

  @Mock(answer = Answers.RETURNS_SELF)
  private ClientBuilder mockClientBuilder;

  @Mock private BasicClient mockClient;
  @Mock private BlockingQueue<String> mockMsgQ;
  private GsonBuilder gsonBuilder;

  @BeforeEach
  void before() {
    gsonBuilder = new GsonBuilder().setDateFormat(TwitterStreamService.TWITTER_DATE_FORMAT);
  }

  @Test
  void filter_maxTwits_success() throws TwitterStreamingException, InterruptedException {
    TwitterClientConfig dummyConfig =
        TwitterClientConfig.builder()
            .consumerKey("someKey")
            .consumerSecret("someSecret")
            .token("someToken")
            .tokenSecret("someTS")
            .build();

    TwitterStreamService service =
        TwitterStreamService.builder()
            .config(dummyConfig)
            .messageQ(mockMsgQ)
            .twitterGson(gsonBuilder.create())
            .twitterClientBuilder(mockClientBuilder)
            .build();

    String[] msgs = fakeTwits(2);
    doReturn(msgs[0], msgs[1]).when(mockMsgQ).poll(anyLong(), eq(TimeUnit.MILLISECONDS));

    doReturn(mockClient).when(mockClientBuilder).build();

    List<Twit> messages = service.filter(60, 2);

    assertEquals(2, messages.size());
  }

  @Test
  void filter_timeout_success() throws TwitterStreamingException, InterruptedException {
    TwitterClientConfig dummyConfig =
        TwitterClientConfig.builder()
            .consumerKey("someKey")
            .consumerSecret("someSecret")
            .token("someToken")
            .tokenSecret("someTS")
            .build();

    TwitterStreamService service =
        TwitterStreamService.builder()
            .config(dummyConfig)
            .messageQ(mockMsgQ)
            .twitterGson(gsonBuilder.create())
            .twitterClientBuilder(mockClientBuilder)
            .build();

    doAnswer(
            (InvocationOnMock invocationOnMock) -> {
              Thread.sleep(100);
              return fakeTwits(1)[0];
            })
        .when(mockMsgQ)
        .poll(anyLong(), eq(TimeUnit.MILLISECONDS));

    doReturn(mockClient).when(mockClientBuilder).build();

    List<Twit> messages = service.filter(490, 100, "someKeyword");

    assertEquals(5, messages.size());
  }

  @Test
  void filter_noTimeout_noMessages() throws TwitterStreamingException {
    TwitterClientConfig dummyConfig =
        TwitterClientConfig.builder()
            .consumerKey("someKey")
            .consumerSecret("someSecret")
            .token("someToken")
            .tokenSecret("someTS")
            .build();

    TwitterStreamService service =
        TwitterStreamService.builder()
            .config(dummyConfig)
            .messageQ(mockMsgQ)
            .twitterGson(gsonBuilder.create())
            .twitterClientBuilder(mockClientBuilder)
            .build();

    doReturn(mockClient).when(mockClientBuilder).build();

    List<Twit> messages = service.filter(-1, 100, "someone", "something");

    assertEquals(0, messages.size());
    verifyZeroInteractions(mockMsgQ);
  }

  @Test
  void filter_noMaxTwits_noMessages() throws TwitterStreamingException {
    TwitterClientConfig dummyConfig =
        TwitterClientConfig.builder()
            .consumerKey("someKey")
            .consumerSecret("someSecret")
            .token("someToken")
            .tokenSecret("someTS")
            .build();

    TwitterStreamService service =
        TwitterStreamService.builder()
            .config(dummyConfig)
            .messageQ(mockMsgQ)
            .twitterGson(gsonBuilder.create())
            .twitterClientBuilder(mockClientBuilder)
            .build();

    doReturn(mockClient).when(mockClientBuilder).build();

    List<Twit> messages = service.filter(1_000, 0, "someone", "something", "sometime");

    assertEquals(0, messages.size());
    verifyZeroInteractions(mockMsgQ);
  }

  @Test
  void filter_interrupted_exception() throws InterruptedException {
    TwitterClientConfig dummyConfig =
        TwitterClientConfig.builder()
            .consumerKey("someKey")
            .consumerSecret("someSecret")
            .token("someToken")
            .tokenSecret("someTS")
            .build();

    TwitterStreamService service =
        TwitterStreamService.builder()
            .config(dummyConfig)
            .messageQ(mockMsgQ)
            .twitterGson(gsonBuilder.create())
            .twitterClientBuilder(mockClientBuilder)
            .build();

    doThrow(new InterruptedException("stopped!"))
        .when(mockMsgQ)
        .poll(anyLong(), eq(TimeUnit.MILLISECONDS));

    doReturn(mockClient).when(mockClientBuilder).build();

    assertThrows(TwitterStreamingException.class, () -> service.filter(10, 10));
  }

  @Test
  void groupTwitsByUser_null_emptyMap() {
    TwitterClientConfig dummyConfig =
        TwitterClientConfig.builder()
            .consumerKey("someKey")
            .consumerSecret("someSecret")
            .token("someToken")
            .tokenSecret("someTS")
            .build();

    TwitterStreamService service =
        TwitterStreamService.builder()
            .config(dummyConfig)
            .messageQ(mockMsgQ)
            .twitterGson(gsonBuilder.create())
            .twitterClientBuilder(mockClientBuilder)
            .build();

    Map<TwitterUser, List<Twit>> userTwitMap = service.groupTwitsByUser(null);

    assertTrue(userTwitMap.isEmpty());
  }

  @Test
  void groupTwitsByUser_multipleTwitsPerUser_success() {
    TwitterClientConfig dummyConfig =
        TwitterClientConfig.builder()
            .consumerKey("someKey")
            .consumerSecret("someSecret")
            .token("someToken")
            .tokenSecret("someTS")
            .build();

    TwitterStreamService service =
        TwitterStreamService.builder()
            .config(dummyConfig)
            .messageQ(mockMsgQ)
            .twitterGson(gsonBuilder.create())
            .twitterClientBuilder(mockClientBuilder)
            .build();

    TwitterUser user1 = new TwitterUser("dummy_01", new Date(), "Dummy 01", "dummy_01");
    TwitterUser user2 = new TwitterUser("dummy_02", new Date(), "Dummy 02", "dummy_02");
    TwitterUser user3 = new TwitterUser("dummy_03", new Date(), "Dummy 03", "dummy_03");

    List<Twit> twitlist = new ArrayList<>(10);
    twitlist.addAll(fakeTwitListForUser(user1, 3));
    twitlist.addAll(fakeTwitListForUser(user2, 1));
    twitlist.addAll(fakeTwitListForUser(user3, 6));

    Map<TwitterUser, List<Twit>> userTwitMap = service.groupTwitsByUser(twitlist);

    assertEquals(3, userTwitMap.size());
    assertEquals(3, userTwitMap.get(user1).size());
    assertEquals(1, userTwitMap.get(user2).size());
    assertEquals(6, userTwitMap.get(user3).size());
  }

  private String[] fakeTwits(int count) {
    String[] fakes = new String[count];

    Gson gson = gsonBuilder.create();

    TwitterUser fakeUser;
    Twit fakeTwit;
    for (int i = 0; i < count; i++) {
      fakeUser =
          new TwitterUser("id_user_str_" + i, new Date(), "real_name_" + i, "screen_name_" + i);
      fakeTwit = new Twit("id_str_" + i, "msg_" + i, new Date(), fakeUser);
      fakes[i] = gson.toJson(fakeTwit);
    }

    return fakes;
  }

  private List<Twit> fakeTwitListForUser(TwitterUser user, int count) {
    List<Twit> fakes = new ArrayList<>(count);
    for (int i = 0; i < count; i++) {
      Twit fakeTwit = new Twit("id_str_" + i, "msg_" + i, new Date(), user);
      fakes.add(fakeTwit);
    }

    return fakes;
  }
}
