package victor.sytac;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.twitter.hbc.ClientBuilder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import lombok.extern.slf4j.Slf4j;
import victor.sytac.config.TwitterClientConfig;
import victor.sytac.exception.TwitterStreamingApplicationException;
import victor.sytac.json.input.Twit;
import victor.sytac.json.input.TwitterUser;
import victor.sytac.json.output.OutputUserTwits;
import victor.sytac.service.TwitOutputService;
import victor.sytac.service.TwitterStreamService;

/** Main entry point for the application. */
@Slf4j
public class TwitterStreamApplication {

  private static final int DEFAULT_QUEUE_SIZE = 64 * 1024;

  static final int DEFAULT_TIMEOUT_SECONDS = 30;
  static final int DEFAULT_MAX_TWITS = 100;

  private final TwitterClientConfig config;
  private final long timeout;
  private final int maxTwits;
  private final String[] keywords;
  private final TwitterStreamService.TwitterStreamServiceBuilder twitterStreamServiceBuilder;
  private final TwitOutputService twitOutputService;

  /**
   * This constructor parse the command line arguments to retrieve the required application config.
   *
   * @param args The command line arguments to be parsed.
   * @param twitterStreamServiceBuilder {@link TwitterStreamService.TwitterStreamServiceBuilder}
   *     used to create {@link TwitterStreamService} instance, used to access Twitter Streaming API.
   * @param twitOutputService {@link TwitOutputService} instance, used to write output JSON file.
   */
  TwitterStreamApplication(
      String[] args,
      TwitterStreamService.TwitterStreamServiceBuilder twitterStreamServiceBuilder,
      TwitOutputService twitOutputService) {
    Map<String, String> argsmap = splitArgs(args);

    config =
        TwitterClientConfig.builder()
            .consumerKey(argsmap.get(Arg.CONSUMER_KEY.name))
            .consumerSecret(argsmap.get(Arg.CONSUMER_SECRET.name))
            .token(argsmap.get(Arg.TOKEN.name))
            .tokenSecret(argsmap.get(Arg.TOKEN_SECRET.name))
            .build();

    timeout = parseTimeout(argsmap);
    maxTwits = parseMaxTwits(argsmap);
    keywords = parseKeywords(argsmap);

    this.twitterStreamServiceBuilder =
        Optional.ofNullable(twitterStreamServiceBuilder)
            .orElseThrow(
                () -> new IllegalArgumentException("twitterStreamServiceBuilder cannot be null."));

    this.twitOutputService =
        Optional.ofNullable(twitOutputService)
            .orElseThrow(() -> new IllegalArgumentException("twitOutputService cannot be null."));
  }

  /** Start the application. */
  void start() throws TwitterStreamingApplicationException {
    Integer qSize = maxTwits > DEFAULT_QUEUE_SIZE || maxTwits < 1 ? DEFAULT_QUEUE_SIZE : maxTwits;
    log.trace(String.format("qSize set to %d", qSize));

    // Filter twits and group them by user.
    Gson twitterGson =
        new GsonBuilder().setDateFormat(TwitterStreamService.TWITTER_DATE_FORMAT).create();

    TwitterStreamService service =
        this.twitterStreamServiceBuilder
            .config(config)
            .twitterClientBuilder(new ClientBuilder())
            .twitterGson(twitterGson)
            .messageQ(new ArrayBlockingQueue<>(qSize))
            .build();

    List<Twit> twits = service.filter(timeout, maxTwits, keywords);
    Map<TwitterUser, List<Twit>> userTwitMap = service.groupTwitsByUser(twits);

    // Write output file.
    Set<OutputUserTwits> output = twitOutputService.convertUserTwitMapToOutputFormat(userTwitMap);
    twitOutputService.writeToOutputJsonFile(output);

    log.info(String.format("Number of Twits filtered on this run: %d", twits.size()));
  }

  /**
   * Main application entry point. Args must be passed on the <code>--key=value</code> format:
   *
   * <ul>
   *   <li>--consumer-key: User consumer key used by the Twitter client
   *   <li>--consumer-secret: User consumer secret used by the Twitter client
   *   <li>--token: App token used by the Twitter client
   *   <li>--token-secret: App token secret key used by the Twitter client
   *   <li>--keywords: Comma separated list of terms to filter Twits
   *   <li>--timeout: Timeout in seconds. Twits will be filtered for at most this amount of time.
   *       Defaults to {@value DEFAULT_TIMEOUT_SECONDS}
   *   <li>--max: Maximum number of Twits to be filtered. Defaults to {@value DEFAULT_MAX_TWITS}
   * </ul>
   *
   * @param args Command line arguments.
   */
  public static void main(String[] args) {
    try {
      TwitterStreamApplication app =
          new TwitterStreamApplication(
              args, TwitterStreamService.builder(), TwitOutputService.builder().build());

      app.start();
    } catch (TwitterStreamingApplicationException ignore) {
      // This should have been logged elsewhere.
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }
  }

  private enum Arg {
    CONSUMER_KEY("--consumer-key"),
    CONSUMER_SECRET("--consumer-secret"),
    TOKEN("--token"),
    TOKEN_SECRET("--token-secret"),
    KEYWORDS("--keywords"),
    TIMEOUT("--timeout"),
    MAX("--max");

    private final String name;

    Arg(String name) {
      this.name = name;
    }
  }

  private Map<String, String> splitArgs(String[] args) {
    Map<String, String> argsmap = new HashMap<>(args.length);
    for (String arg : args) {
      String[] brokenArg = arg.split("=");
      if (brokenArg.length == 2) {
        argsmap.put(brokenArg[0], brokenArg[1]);
      }
    }

    return argsmap;
  }

  private <T> T parseArg(
      Function<String, T> parser, Map<String, String> argsmap, Arg arg, T defVal) {
    try {
      return Optional.ofNullable(parser.apply(argsmap.get(arg.name))).orElse(defVal);
    } catch (Exception ignore) {
      // Null value, unparseable value, etc. If it can't be parsed, return default.
      log.trace(
          String.format(
              "Cannot parse %s argument with value %s. Will use default value.",
              arg.name, argsmap.get(arg.name)));
      return defVal;
    }
  }

  private long parseTimeout(Map<String, String> argsmap) {
    return parseArg(
        s -> TimeUnit.SECONDS.toMillis(Integer.parseInt(s)),
        argsmap,
        Arg.TIMEOUT,
        TimeUnit.SECONDS.toMillis(DEFAULT_TIMEOUT_SECONDS));
  }

  private int parseMaxTwits(Map<String, String> argsmap) {
    return parseArg(Integer::parseInt, argsmap, Arg.MAX, DEFAULT_MAX_TWITS);
  }

  private String[] parseKeywords(Map<String, String> argsmap) {
    return parseArg(s -> s.split(","), argsmap, Arg.KEYWORDS, new String[] {});
  }
}
