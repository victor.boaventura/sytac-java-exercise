package victor.sytac.config;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

/** Configuration for the Twitter client. */
@Value
@Builder(toBuilder = true)
public class TwitterClientConfig {
  @NonNull private String consumerKey;
  @NonNull private String consumerSecret;
  @NonNull private String token;
  @NonNull private String tokenSecret;
}
