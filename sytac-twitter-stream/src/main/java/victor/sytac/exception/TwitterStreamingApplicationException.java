package victor.sytac.exception;

/**
 * Abstract exception used to type all other exceptions thrown by this application. If an exception
 * of this type was thrown, the error should have been logged already.
 */
public abstract class TwitterStreamingApplicationException extends Exception {

  /** @param message Error message for exception. */
  TwitterStreamingApplicationException(String message) {
    super(message);
  }

  /**
   * @param message Error message for exception.
   * @param cause {@link Throwable} object that this exception encapsulates.
   */
  TwitterStreamingApplicationException(String message, Throwable cause) {
    super(message, cause);
  }
}
