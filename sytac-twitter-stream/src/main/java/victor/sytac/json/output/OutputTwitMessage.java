package victor.sytac.json.output;

import java.util.Date;
import lombok.Value;
import victor.sytac.json.input.Twit;

@Value
public class OutputTwitMessage {
  private String twitterMessageId;
  private String text;
  private Date creationDate;

  public OutputTwitMessage(Twit twit) {
    this.twitterMessageId = twit.getTwitterMessageId();
    this.text = twit.getText();
    this.creationDate = twit.getCreationDate();
  }
}
