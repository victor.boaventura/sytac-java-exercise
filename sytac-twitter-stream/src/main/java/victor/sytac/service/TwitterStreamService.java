package victor.sytac.service;

import com.google.gson.Gson;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.endpoint.StreamingEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import lombok.Builder;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import victor.sytac.config.TwitterClientConfig;
import victor.sytac.exception.TwitterStreamingException;
import victor.sytac.json.input.Twit;
import victor.sytac.json.input.TwitterUser;

/** Service class that contains methods to retrieve real time twits. */
@Builder()
@Slf4j
public class TwitterStreamService {

  public static final String TWITTER_DATE_FORMAT = "EEE MMM dd HH:mm:ss Z yyyy";

  @NonNull private final TwitterClientConfig config;

  @NonNull private final ClientBuilder twitterClientBuilder;

  @NonNull private final Gson twitterGson;

  @NonNull private final BlockingQueue<String> messageQ;

  /**
   * Filter real time twits.
   *
   * @param timeout Amount of milliseconds to get twits in real time.
   * @param maxTwits Maximum twits to be get.
   * @param keywords {@link List} of strings containing keywords to filter twits in real time.
   * @return {@link List} of filtered twits.
   * @throws TwitterStreamingException When an error occurs while filtering twits.
   */
  public List<Twit> filter(long timeout, int maxTwits, String... keywords)
      throws TwitterStreamingException {

    // Declaring host and endpoint to connect to.
    Hosts hbcHosts = new HttpHosts(Constants.STREAM_HOST);
    StreamingEndpoint endpoint = new StatusesFilterEndpoint().trackTerms(Arrays.asList(keywords));

    // Set up authentication based on configuration.
    Authentication hbcAuth =
        new OAuth1(
            config.getConsumerKey(),
            config.getConsumerSecret(),
            config.getToken(),
            config.getTokenSecret());

    // Create Twitter client.
    Client client =
        twitterClientBuilder
            .hosts(hbcHosts)
            .authentication(hbcAuth)
            .endpoint(endpoint)
            .processor(new StringDelimitedProcessor(messageQ))
            .build();

    // Attempts to establish a connection.
    client.connect();

    // Retrieve twits.
    try {
      return pollTwits(timeout, maxTwits);
    } finally {
      // Close resources.
      client.stop();
    }
  }

  /**
   * Group twits by user.
   *
   * @param twits {@link Collection} of twits to be grouped by user.
   * @return {@link Map} of each user to a collection of twits.
   */
  public Map<TwitterUser, List<Twit>> groupTwitsByUser(Collection<Twit> twits) {
    Map<TwitterUser, List<Twit>> userTwitMap = new HashMap<>();

    if (twits != null) {
      twits.forEach(twit -> addTwitToUserTwitMap(userTwitMap, twit));
    }

    return userTwitMap;
  }

  private List<Twit> pollTwits(long timeout, int maxTwits) throws TwitterStreamingException {

    try {
      List<Twit> twits = new ArrayList<>(maxTwits);
      while (timeout > 0 && twits.size() < maxTwits) {
        long time = System.currentTimeMillis();
        String msg = messageQ.poll(timeout, TimeUnit.MILLISECONDS);
        timeout -= (System.currentTimeMillis() - time);

        if (msg != null && !msg.isEmpty()) {
          Twit twit = twitterGson.fromJson(msg, Twit.class);

          if (twit.getCreationDate() == null
              || twit.getAuthor() == null
              || twit.getAuthor().getCreatedAt() == null
              || twit.getAuthor().getName() == null
              || twit.getText() == null) {
            log.warn(String.format("Ignoring message, could not parse Twit: %s", msg));
          } else {
            twits.add(twit);
            log.trace(String.format("Twitter message polled: %s", twit.toString()));
          }
        }
      }
      return twits;
    } catch (InterruptedException e) {
      log.error(e.getMessage(), e);
      throw new TwitterStreamingException("Interrupted when polling twits.", e);
    }
  }

  private void addTwitToUserTwitMap(Map<TwitterUser, List<Twit>> userTwitMap, Twit twit) {
    TwitterUser user = twit.getAuthor();
    userTwitMap.computeIfAbsent(user, twitterUser -> new LinkedList<>()).add(twit);
  }
}
