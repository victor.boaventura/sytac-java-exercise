package victor.sytac.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import victor.sytac.json.input.Twit;
import victor.sytac.json.input.TwitterUser;
import victor.sytac.json.output.OutputTwitMessage;
import victor.sytac.json.output.OutputUserTwits;

/** Service used to generate twit JSON report. */
@Builder()
@Slf4j
public class TwitOutputService {

  private static final String OUTPUT_CHARSET = "UTF-8";
  private static final String OUTPUT_FILE = "./output/twitter-stream.json";

  /**
   * Converts a map of user twits to the expected output format.
   *
   * @param userTwitMap {@link Map} of {@link Twit} per {@link TwitterUser}.
   * @return {@link Set} of {@link OutputUserTwits}. The set is ordered by the user creation date,
   *     as are the twit messages inside each user.
   */
  public Set<OutputUserTwits> convertUserTwitMapToOutputFormat(
      Map<TwitterUser, List<Twit>> userTwitMap) {
    Set<OutputUserTwits> result =
        new TreeSet<>(
            Comparator.comparingLong(outputUserTwits -> outputUserTwits.getCreatedAt().getTime()));

    if (userTwitMap != null) {
      userTwitMap.forEach(
          ((twitterUser, twits) -> {
            // Create ordered collection of twit messages.
            Set<OutputTwitMessage> outputTwitMessages =
                new TreeSet<>(
                    Comparator.comparingLong(
                        outputTwitMessage -> outputTwitMessage.getCreationDate().getTime()));

            // Convert each twit to output format.
            twits
                .stream()
                .map(OutputTwitMessage::new)
                .collect(Collectors.toCollection(() -> outputTwitMessages));

            // Add user to result collection.
            result.add(new OutputUserTwits(twitterUser, outputTwitMessages));
          }));
    }

    return result;
  }

  /**
   * Write output set to a JSON file.
   *
   * @param output {@link Set} of {@link OutputUserTwits} that is to be parsed into JSON and written
   *     to output file.
   */
  public void writeToOutputJsonFile(Set<OutputUserTwits> output) {
    try {
      Gson outputGson = new GsonBuilder().create();
      Files.write(
          Paths.get(OUTPUT_FILE),
          outputGson.toJson(output).getBytes(Charset.forName(OUTPUT_CHARSET)));
    } catch (IOException e) {
      log.error("Error when writing output file.", e);
    }
  }
}
